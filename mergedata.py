#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 14:51:08 2017

@author: max_kyliu
"""

import pandas as pd
import json
import ast
import numpy as np

company = pd.read_csv('company.csv', encoding='latin-1')
#person = pd.read_csv('person.csv', encoding='latin-1')
#product = pd.read_csv('product.csv', encoding='latin-1')
#service-provider = pd.read_csv('service-provider.csv', encoding='latin-1')

current_year = 2017
current_month = 11

newDataset = company[['name','category_code','number_of_employees']].copy()
#company['founded_year'] = company['founded_year'].fillna(current_year)
#company['founded_month'] = company['founded_month'].fillna(current_month)
company['milestones'] = company['milestones'].fillna('[]')
company['offices'] = company['offices'].fillna('[]')
company['products'] = company['products'].fillna('[]')
company['providerships'] = company['providerships'].fillna('[]')
company['competitions'] = company['competitions'].fillna('[]')
company['funding_rounds'] = company['funding_rounds'].fillna('[]')
newDataset['company_age'] = (current_year - company['founded_year'])*12 + (company['founded_month'] - current_month)

newDataset['number_of_milestones'] = 0
newDataset['number_of_offices'] = 0
newDataset['number_of_products'] = 0
newDataset['number_of_providers'] = 0
newDataset['number_of_competitors'] = 0
newDataset['number_of_acq_competitors'] = 0
newDataset['acquired_funded_ipo'] = 0

for index, row in company.iterrows():

    newDataset['number_of_milestones'][index] = len(ast.literal_eval(row['milestones']))

    newDataset['number_of_offices'][index] = len(ast.literal_eval(row['offices']))

    newDataset['number_of_products'][index] = len(ast.literal_eval(row['products']))

    newDataset['number_of_providers'][index] = len(ast.literal_eval(row['providerships']))

    competitors = ast.literal_eval(row['competitions'])
    newDataset['number_of_competitors'][index] = len(competitors)

    number_of_acq_competitors = 0
    for competitor in competitors:
        competitor_name = competitor.get('competitor').get('name')
        if(competitor_name):
            s = company.loc[company['name'] == competitor_name]
            if(s['acquisition'].any()):
                number_of_acq_competitors = number_of_acq_competitors + 1
    
    newDataset['number_of_acq_competitors'][index] = number_of_acq_competitors
 
#newDataset['number_of_articles']

#newDataset['founders_exp']
#newDataset['number_of_founders_companies']
#newDataset['founders_funded_companies']

    if(pd.isnull(row['acquisition'])):
        acquired = 0
    else:
        acquired = 1
    if(pd.isnull(row['ipo'])):
        ipo = 0
    else:
        ipo = 1
    funded = len(ast.literal_eval(row['funding_rounds']))
    if (acquired + funded + ipo) > 0:
        newDataset['acquired_funded_ipo'][index] = 1
    else:
        newDataset['acquired_funded_ipo'][index] = 0
        
newDataset.to_csv('output.csv')
