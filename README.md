Based on Crunchbase database for ANN training.

Comparation of the different algo:

>>>
500 companies result:

K-NN
[[90 15]
 [12  8]]

SVM
[[103   2]
 [ 18   2]]

Kernel SVM
[[103   2]
 [ 18   2]]

Naive Bayes
[[58 47]
 [ 5 15]]

Decision Tree
[[85 20]
 [11  9]]

Random Forest
[[89 16]
 [11  9]]

ANN
Epoch 100/100
375/375 [==============================] - 0s 243us/step - loss: 0.3189 - acc: 0.8533
[[93 12]
 [15  5]]

(best_parameters)
{'epochs': 100, 'optimizer': 'rmsprop', 'batch_size': 52}
(best_accuracy)
0.797333333333

Epoch 100/100
375/375 [==============================] - 0s 64us/step - loss: 0.3845 - acc: 0.8267
print(cm)
[[101   4]
 [ 17   3]]

79150 company result
Epoch 100/100
59362/59362 [==============================] - 2s 38us/step - loss: 0.4675 - acc: 0.7817

(cm)
[[13659  1008]
 [ 3364  1757]]
