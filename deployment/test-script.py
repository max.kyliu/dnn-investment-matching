import numpy as np
import pandas as pd
import keras.models
from keras.models import model_from_json
import json

json_file = open('classifier.json','r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
#load woeights into new model
loaded_model.load_weights("model.h5")
print("Loaded Model from disk")

#classifier = load_model('company_valuation_trained.h5')
#print("Loaded all data")

#compile and evaluate loaded model
loaded_model.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

#loss,accuracy = model.evaluate(X_test,y_test)
#print('loss:', loss)
#print('accuracy:', accuracy)

# Predicting the Test set results
x_test = pd.read_json('test.json', orient='records')
y_pred = loaded_model.predict(x_test)
y_pred = (y_pred > 0.5)
print(y_pred)


