import json
import requests

#Setting the headers to send and accept json responses
header = {'Content-Type': 'application/json', \
                  'Accept': 'application/json'}

#Reading test batch
json_file = open('predict.json','r')
data = json_file.read()
json_file.close()

#Converting Pandas Dataframe to json
data = df.to_json(orient='records')

print(data)

#POST <url>/predict
resp = requests.post("http://0.0.0.0:8000/predict", \
                    data = json.dumps(data),\
                    headers= header)

print(resp.status_code)

print(resp.json())
