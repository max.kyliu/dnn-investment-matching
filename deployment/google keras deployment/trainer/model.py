# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Implements the Keras Sequential model."""

import itertools

import keras
import pandas as pd
from keras import backend as K
from keras import layers, models
from keras.utils import np_utils
from keras.backend import relu, sigmoid

#from urlparse import urlparse
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.preprocessing import StandardScaler

import tensorflow as tf
from tensorflow.python.saved_model import builder as saved_model_builder
#from tensorflow.python.saved_model import utils
from tensorflow.python.saved_model import tag_constants, signature_constants
from tensorflow.python.saved_model.signature_def_utils_impl import build_signature_def, predict_signature_def
#from tensorflow.contrib.session_bundle import exporter

# csv columns in the input file
CSV_COLUMNS = (' ', 'name','category_code', 'number_of_employees', 'company_age', 'number_of_milestones',
               'number_of_offices', 'number_of_products', 'number_of_providers', 'number_of_competitors',
               'number_of_acq_competitors', 'acquired_funded_ipo')

CSV_COLUMN_DEFAULTS = [[0], [''], [''], [0], [0], [0], [0], [0], [0], [0],
                       [0], [0]]

# Categorical columns with vocab size
CATEGORICAL_COLS = (('category_code', 19),)

CATEGORIES = ['advertising','software','network_hosting','mobile','ecommerce','consulting','web','biotech','other','education','search','enterprise','games_video','public_relations','hardware','cleantech','security','legal','semiconductor']

CONTINUOUS_COLS = ('number_of_employees', 'company_age', 'number_of_milestones',
               'number_of_offices', 'number_of_products', 'number_of_providers', 'number_of_competitors',
               'number_of_acq_competitors')

#LABELS = [0, 1]
LABEL_COLUMN = 'acquired_funded_ipo'

UNUSED_COLUMNS = set(CSV_COLUMNS) - set(
    list(zip(*CATEGORICAL_COLS))[0] + CONTINUOUS_COLS + (LABEL_COLUMN,))


def model_fn(input_dim,
             labels_dim,
             learning_rate=0.1):
  """Create a Keras Sequential model with layers."""
  model = models.Sequential()

  model.add(layers.Dense(units = 13, kernel_initializer = 'uniform', activation = 'relu', input_dim = input_dim))
  model.add(layers.Dense(units = 13, kernel_initializer = 'uniform', activation = 'relu'))
  model.add(layers.Dense(units = labels_dim, kernel_initializer = 'uniform', activation = 'sigmoid'))

  compile_model(model, learning_rate)
  return model

def compile_model(model, learning_rate):
  #model.compile(loss='binary_crossentropy',
  model.compile(loss='categorical_crossentropy',
                optimizer='adam', 
                #optimizer=keras.optimizers.SGD(lr=learning_rate),
                metrics=['accuracy'])
  return model

def to_savedmodel(model, export_path):
  """Convert the Keras HDF5 model into TensorFlow SavedModel."""

  builder = saved_model_builder.SavedModelBuilder(export_path)

  signature = predict_signature_def(inputs={'input': model.inputs[0]},
                                    outputs={'worth_invest': model.outputs[0]})

  with K.get_session() as sess:
    builder.add_meta_graph_and_variables(
        sess=sess,
        tags=[tag_constants.SERVING],
        signature_def_map={
            signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: signature}
    )
    builder.save()


def to_numeric_features(features):
  """Convert the pandas input features to numeric values.
     Args:
        features: Input features in the data
  """

  for col in CATEGORICAL_COLS:
    features = pd.concat([features, pd.get_dummies(features[col[0]], drop_first = True).reindex(columns=CATEGORIES, fill_value=0)], axis = 1)
    features.drop(col[0], axis = 1, inplace = True)

  # Remove the unused columns from the dataframe
  for col in UNUSED_COLUMNS:
    features.pop(col)

  return features

def generator_input(input_file, chunk_size):
  """Generator function to produce features and labels
     needed by keras fit_generator.
  """
  #input_reader = pd.read_csv(tf.gfile.Open(input_file[0]),
  #                         names=CSV_COLUMNS,
  #                         chunksize=chunk_size,
  #                         na_values=" ?")
  input_reader = pd.read_csv(input_file[0],
                           names=CSV_COLUMNS,
                           chunksize=chunk_size)

  for input_data in input_reader:
    # specific hard code
    # Taking care of missing data
    input_data['category_code'] = input_data['category_code'].fillna(value='None')
    input_data['number_of_employees'] = input_data['number_of_employees'].fillna(value=0)
    input_data['company_age'] = input_data['company_age'].fillna(value=0)
    input_data = input_data.dropna()
    label = pd.get_dummies(input_data.pop(LABEL_COLUMN))

    input_data = to_numeric_features(input_data).astype(int)
    
    #input_data['category_code'] = input_data['category_code'].fillna('None')
    #input_data['number_of_employees'] = input_data['number_of_employees'].fillna(value=0)
    #input_data['company_age'] = input_data['company_age'].fillna(value=0)
    #X = input_data.iloc[:, 2:10].values
    #y = input_data.iloc[:, 11].values

    # Taking care of missing data
    #imputer = Imputer(missing_values = 'NaN', strategy = 'median', axis = 0)
    #imputer.fit(X[:, 1:3]
    #X[:, 1:3] = imputer.transform(X[:, 1:3])

    # Encoding categorical data
    #labelencoder_X_1 = LabelEncoder()
    #X[:, 0] = labelencoder_X_1.fit_transform(X[:, 0])
    #onehotencoder = OneHotEncoder(categorical_features = [0])
    #X = onehotencoder.fit_transform(X).toarray()

    # Feature Scaling
    #sc = StandardScaler()
    #X = sc.fit_transform(X)
    
    n_rows = input_data.shape[0]
    return ( (input_data.iloc[[index % n_rows]], label.iloc[[index % n_rows]]) for index in itertools.count() )
