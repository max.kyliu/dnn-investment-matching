import pickle
import json
import csv
from os import listdir
from os.path import isfile, join

class EntityParser:
	
	@staticmethod
	def LoadJsonEntity(filename):
		text = EntityParser.LoadStringEntityByFilename(filename)
		js = None
		try:
			if text:
				js = json.loads(text, strict=False)
		except ValueError as e:
			print(e)
		return js

	@staticmethod
	def LoadStringEntityByFileHandler(fid):
		if fid:
			return pickle.load(fid)
		else:
			return ''

	@staticmethod
	def LoadStringEntityByFilename(filename, mode = 'rb'):
         with open(filename, mode) as fid:
        		obj = None
        		if fid:
        			try:
        				obj = pickle.load(fid)
        			except ValueError as e:
        				obj = None
         return obj
	
	
def main():
    
    outputfile = 'company.csv'
    dirpath = './company/'
    arr = listdir(dirpath)
    index = 0
    
    for files in arr:
        print(dirpath + files)
        if index != 0:
            js = EntityParser.LoadJsonEntity(dirpath + files)            
            if(js):
                if index == 0:
                    index = index + 1
                elif index == 1:                    
                    with open(outputfile, 'w', encoding='utf-8') as f:  # Just use 'w' mode in 3.x
                        w = csv.DictWriter(f, js.keys())
                        w.writeheader()
                        w.writerow(js)
                        index = index + 1
                else:   
                    with open(outputfile, 'a', encoding='utf-8') as f:  # Just use 'w' mode in 3.x
                        w = csv.DictWriter(f, js.keys())
                        w.writerow(js)
                        index = index + 1
        else:
            index = index + 1

if __name__ == "__main__":
	main()
